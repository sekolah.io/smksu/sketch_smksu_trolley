/**********************************************************
 * Human Following Trolley - Trolley System
 * by Sekolah Menengah Kebangsaan Saujana Utama
 * 
 * Team:
 *    1) Ameer
 *    2) Ghazi
 *    3) Khalish
 *    4) Ambreen
 *    
 * Manager:
 *    1) Hamidah Mukhtar
 *    2) 
 *    
 * Technical Coach:
 *    1) Sallehuddin Abdul Latif <salleh@hexabit.io>
 * 
 * Copyright (C) 2019 SMKSU
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Define all constant for the program

// To allow different motor control (so that we can make left and right turn)
// we need 2 set of pin input (3 pins per motor - control, in1, in2) for L298N

// For Motor A (right wheel)
#define motorA_Control  3 //must be PWM
#define motorA_Input1   2
#define motorA_Input2   4

// For Motor B (left wheel)
#define motorB_Control  6 //must be PWM
#define motorB_Input1   5
#define motorB_Input2   7


// To measure distance and direction, we need at least 2 HC-SR04.
// Each need 1 output for trigger and 1 input for echo reading
// However, as we need to activate the trigger simultaneously
// for both sensor, then they can share the same trigger pin.
// Hence we only need 3 pins tu use for 2 HC-SR04

#define sonar_Trigger 10
#define sonarL_Echo   9
#define sonarR_Echo   8

// To receive trigger signal from belt, we are using NRF24L01+ tranceiver.
// It use 5 pin
/*
#define radio_SCK   13
#define radio_MISO  12
#define radio_MOSI  11
#define radio_CE    ? //alamak, tak cukup (tak nak guna pin 0 & 1 as it should be reserved for serial communication with the board)
#define radio_CSN   ? //alamak, tak cukup
*/

// Alternative - using RF 315MHz or 433MHz transmitter-receiver module
// 1 transmitter, and one receiver
#define radio_In  11
#define radio_Out 12

// Signals
const int OK            = 0;
const int READY         = 1;
const int TOO_FAR       = 2;
const int OBSTRUCTED    = 3;
const int RADIO_FAILURE = 4;

void setup() {
  // put your setup code here, to run once:
  set_pin_mode();

  // First make sure all motors are stopped
  stopAllMotors();

  // then start the RF receiver to listen for timing signal from Belt system
  

}

void loop() {
  // put your main code here, to run repeatedly:

  /*
   * 1) Mula-mula kita wait signal dari belt untuk start check berapa jauh trolley dari belt
   * 
   * 2) Lepas dapat tahu berapa jauh dan direction, kita set motor untuk bergerak berdasarkan
   * kiraan dari input sonar tadi.
   * 
   * 3) Dah start gerak, kita hantar pula message kepada belt
   *    a) ok.. tak ada masalah
   *    b) alamak... ada halangan?
   *    c) wei... napa jauh sangat... tunggu kejap.
   * 
   */

  // wait for signal from belt
  
  // once got signal, start measure distance and compare the 2 distances to estimate direction

  // decide to move forward, reverse, turn left or turn right

  // tell Belt status
  send_status("current");
}

void set_pin_mode() {
  // Motor A pins
  pinMode(motorA_Control, OUTPUT);
  pinMode(motorA_Input1, OUTPUT);
  pinMode(motorA_Input2, OUTPUT);

  // Motor B pins
  pinMode(motorA_Control, OUTPUT);
  pinMode(motorA_Input1, OUTPUT);
  pinMode(motorA_Input2, OUTPUT);

  // HC-SR04 pins
  pinMode(sonar_Trigger, OUTPUT);
  pinMode(sonarL_Echo, INPUT);
  pinMode(sonarR_Echo, INPUT);

  // Radio pins
  pinMode(radio_In, INPUT);
  pinMode(radio_Out, OUTPUT);
}

void stopAllMotors() {
  // code to set all pin related to L298N to LOW (0 Volt)
  
}

void go_forward() {
  // code to set the motor to move forward
  
}

void go_reverse() {
  // code to set the motor to move backward
  
}

void turn_left(int angle) {
  // code to set the robot to turn left
  
}

void turn_right(int angle) {
  // code to set the robot to turn right
  
}

void send_status(int apekehei) {
  //set the message to send to belt
  switch (apekehei) {
    case 1:
      // we are ok
      break;
    case 2:
      // oops, something wrong... ada halangan?
      break;
    case 3:
      // Distance is too far (over 60cm?)... wait and sound the buzzer
      break;
    default:
      // should we do something?
      break;
  }

  //send the message
}

int get_belt_distance() {
  // 
  // from 2 HC-SR04 signal value, calculate the distance of each

  // subtract right distance from left distance.

  // return the value. If right is closer, then the value is +ve, otherwise -ve
}
